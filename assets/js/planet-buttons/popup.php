<?php
// this file contains the contents of the popup window
	$source = "insert";
	$insert_text = "Insert";
	$title = "Planet Button";
	
    if(isset($_GET['ver']))
	{
		$planet_plugin_ver = htmlspecialchars($_GET['ver']);
	}
	else
	{
		$planet_plugin_ver = "";
	}
	
	
	if(isset($_GET['source']))
	{
		if($_GET['source']=="click")
		{
			$source = "click";
			$title = "Edit Button";
			$insert_text = "Update";
		}
	}
	
	$ajax_url = urldecode($_GET['ajaxurl']);
	$ajax_url = filter_var($ajax_url, FILTER_VALIDATE_URL, FILTER_FLAG_PATH_REQUIRED);
	
	
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><?php echo $title; ?></title>
<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script language="javascript" type="text/javascript" src="../../../../../../wp-includes/js/tinymce/tiny_mce_popup.js?ver=<?php echo $planet_plugin_ver; ?>"></script>
<link rel="stylesheet" href="../../css/button-styles.css?ver=<?php echo $planet_plugin_ver; ?>" />
<link rel="stylesheet" href="../../css/font-awesome.css?ver=<?php echo $planet_plugin_ver; ?>" />
<script src="jquery.minicolors.min.js?ver=<?php echo $planet_plugin_ver; ?>"></script>
<link rel="stylesheet" href="jquery.minicolors.css?ver=<?php echo $planet_plugin_ver; ?>">
<link rel="stylesheet" href="popup.css?ver=<?php echo $planet_plugin_ver; ?>">
<script type="text/javascript">
var source = "<?php echo $source; ?>"; 
var ajax_url = "<?php echo $ajax_url; ?>"; 
</script>
<script type="text/javascript" src="popup.min.js?ver=<?php echo $planet_plugin_ver; ?>"></script>
</head>
<body>

	<!-- http://www.problogdesign.com/wordpress/user-friendly-short-codes-with-tinymce/ -->
	<!-- http://www.tinymce.com/develop/bugtracker_view.php?id=5575 -->
	
	<div id="button-dialog">
		<div class="preview-button-area" style="position:relative;">
			<div class="centered-button">
				
			</div>
			<a href="#" class="fa fa-save save-btn"></a>
		</div>
		
		<form action="/" method="get" accept-charset="utf-8">
			
			<div id="tab-header">
				<ul>
					<li class="active"><a href="#tab-1-content"> Planet Properties</a></li>
					</ul><div class="clear"></div>
			</div>
			
			<div id="tab-1-content" class="planet-tab-content">
				<div class="inputrow main">
					<label for="button-text">Button</label>
					<div class="inputwrap">
						<input type="text" name="button-text" value="" id="button-text" value="" placeholder="Enter your button text&hellip;" />
					</div>
					<div class="clear"></div>
				</div>
				<div class="inputrow main">
					<label for="button-url">URL</label>
					<div class="inputwrap">
						<input type="text" name="button-url" value="" id="button-url" /><br />
					</div>
					<div class="inputwrap">
						<label for="new-window"><small>Open link in new window?</small></label> <input type="checkbox" id="new-window" name="new-window" value="1" />
					</div>
					<div class="clear"></div>
				</div>
				
				<div class="inputrow">
					
					<div class="inputcol left">
						<div class="inputrowc">
							<label for="text-color">Text Color</label>
							<div class="inputwrap">
								<input type="text" id="text-color" name="text-color" value="#000000" />
							</div>
							<div class="clear"></div>
						</div>
						<div class="inputrowc">
							<label for="button-type">Type</label>
							<div class="inputwrap">
								<select name="button-type" id="button-type">
									<option value="planet-type-flat" selected="selected">Flat</option>
									<option value="planet-type-flat planet-rounded-medium">Flat Rounded</option>
									<option value="iphone-red">Switch</option>					
									<option value="ghost-flat">Ghost Button</option>
									

								</select>
							</div>
							<div class="clear"></div>
						</div>
					</div>
					<div class="inputcol right">
						
						<div class="inputrowc">
							<label for="button-color">Button Color</label>
							<div class="inputwrap">
								<input type="text" id="button-color" name="button-color" value="#ffffff" />
							</div>
							<div class="clear"></div>
						</div>
						<div class="inputrowc">
							<label for="button-size">Size</label>
							<div class="inputwrap">
								<select name="button-size" id="button-size" size="1">
									<option value="planet-size-xsmall">Extra Small</option>
									<option value="planet-size-small">Small</option>
									<option value="planet-size-medium" selected="selected">Medium</option>
									<option value="planet-size-large">Large</option>
									<option value="planet-size-xlarge">Extra Large</option>
								</select>
							</div>
							<div class="clear"></div>
						</div>
					</div>
					
			</div>
				
				</div>
			<div id="planet-footer">	
				<a href="javascript:ButtonDialog.insert(ButtonDialog.local_ed)" id="insert" style="display: block; line-height: 24px;"><?php echo $insert_text; ?></a>
			</div>
			
			
		</form>
	</div>
	
</body>
</html>