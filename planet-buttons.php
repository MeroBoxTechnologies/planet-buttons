<?php
/**
 * @link              http://demoify.com/
 * @since             1.0.0
 * @package           Planet_Buttons
 *
 * @wordpress-plugin
 * Plugin Name:       Planet Buttons
 * Plugin URI:        http://demoify.com/plugin/planet-buttons
 * Description:       This is a free WordPress plugin for awesome buttons. 
 * Version:           1.0.0
 * Author:            Demoify
 * Author URI:        http://demoify.com/
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       planet-buttons
 * Domain Path:       /languages
 */

/*
* Define Globals in Plugin
*/
if (!defined('PLANET_BUTTONS_VERSION_NUM'))
    define('PLANET_BUTTONS_VERSION_NUM', '1.0.0');
	
if (!defined('PLUGIN_SLUG'))
    define('PLUGIN_SLUG', 'planet-buttons');

if (!defined('PLANET_BUTTONS_THEME_DIR'))
    define('PLANET_BUTTONS_THEME_DIR', ABSPATH . 'wp-content/themes/' . get_template());

if (!defined('PLANET_BUTTONS_PLUGIN_NAME'))
    define('PLANET_BUTTONS_PLUGIN_NAME', trim(dirname(plugin_basename(__FILE__)), '/'));

if (!defined('PLANET_BUTTONS_PLUGIN_DIR'))
    define('PLANET_BUTTONS_PLUGIN_DIR', WP_PLUGIN_DIR . '/' . PLANET_BUTTONS_PLUGIN_NAME);

if (!defined('PLANET_BUTTONS_PLUGIN_URL'))
    define('PLANET_BUTTONS_PLUGIN_URL', WP_PLUGIN_URL . '/' . PLANET_BUTTONS_PLUGIN_NAME);

if (!defined('PLANET_BUTTONS_BASENAME'))
    define('PLANET_BUTTONS_BASENAME', plugin_basename(__FILE__));

if (!defined('PLANET_BUTTONS_VERSION_KEY'))
    define('PLANET_BUTTONS_VERSION_KEY', 'planetbuttons_version');


add_option(PLANET_BUTTONS_VERSION_KEY, PLANET_BUTTONS_VERSION_NUM);


/*
* Set up Plugin Globals
*/
if ( ! class_exists( 'planetButtons' ) )
{
	class PlanetButtons
	{
		
		public function __construct()
		{			
			//admin styles
			add_action( 'admin_enqueue_scripts', array($this, 'planet_admin_styles') );
			add_filter( 'mce_css', array($this,'plugin_mce_css') );
			
			//admin script
			add_action('init', array($this,'tiny_mce_planet_button'));
			
			//regular styles
			add_action( 'wp_print_styles', array($this, 'planet_styles') );
			
			//Admin Ajax
			add_action( 'wp_ajax_planet_buttons', array($this, 'planet_buttons') ); //if logged in
			
			add_action('admin_head', array($this, 'my_add_styles_admin'));
		}
		
		public function my_add_styles_admin() {

			global $current_screen;
			$type = $current_screen->post_type;
			
			$args = array(
				'public'   => true
			);
			
			$post_types = get_post_types( $args, 'names' );
			
			if (is_admin() && in_array($type, $post_types) ) {
				?>
				<script type="text/javascript">
				var planet_ajaxurl  = '<?php echo admin_url( 'admin-ajax.php' ); ?>';
				var planet_ver  = '<?php echo PLANET_BUTTONS_VERSION_NUM; ?>';
				</script>
				<?php
			}
		}
		public function planet_buttons()
		{
			if($_GET['load']=="save_button")
			{
				
				$buttons = get_option('planet-buttons'); //get existing buttons
				
				if(!is_array($buttons))
				{
					$buttons = array();
				}
				
				$button_html = $_POST['button'];
				if($button_html!="")
				{
					
					$button_html = stripslashes(wp_filter_post_kses($button_html));
					
					$button_number = count($buttons)+1;
					
					$button_data = array();
					$button_data['name'] = "Button ".$button_number;
					$button_data['html'] = $button_html;
					
					array_push($buttons, $button_data);
					
				}
				
				//update_user_meta(get_current_user_id(), 'planet-buttons', $buttons);
				update_option('planet-buttons', $buttons);
				
				$buttons = array_reverse($buttons);
				
				echo json_encode($buttons);
			}
			else if($_GET['load']=="get_buttons")
			{
				//$buttons = get_user_meta(get_current_user_id(), 'planet-buttons', true); //get existing buttons
				$buttons = get_option('planet-buttons'); //get existing buttons
				
				if(!is_array($buttons))
				{
					$buttons = array();
				}
				
				$buttons = array_reverse($buttons);
				
				echo json_encode($buttons);
			}
			else if($_GET['load']=="remove_button")
			{
				//$buttons = get_user_meta(get_current_user_id(), 'planet-buttons', true); //get existing buttons
				$buttons = get_option('planet-buttons'); //get existing buttons
				
				if(!is_array($buttons))
				{
					$buttons = array();
				}
				
				$buttons = array_reverse($buttons);
				
				$removeIndex = (int)$_GET['index'];
				
				unset($buttons[$removeIndex]);
				
				$newButtons = array_reverse($buttons);
				/* updat planet_buttons */
				update_option('planet-buttons', $newButtons);
				
				echo json_encode($buttons);
			}
			else if($_GET['load']=="update_button")
			{
				
				$buttons = get_option('planet-buttons'); /* get existing buttons */
				
				if(!is_array($buttons))
				{
					$buttons = array();
				}
				
				$buttons = array_reverse($buttons);
				
				$renameIndex = (int)$_GET['index'];
				
				$name = esc_attr($_GET['name']);
				$buttons[$renameIndex]['name'] = $name;
				
				$newButtons = array_reverse($buttons);
				/*update_user_meta(get_current_user_id(), 'planet-buttons', $newButtons);*/
				update_option('planet-buttons', $newButtons);
				
				echo json_encode($buttons);
			}
			else
			{
				$msg = array();
				$msg['error'] = "1";
				
				echo json_encode($msg);
			}
			
			exit;
		}
		
				
		public function planet_styles()
		{
			wp_enqueue_style( 'planet-buttons-style', plugins_url( '/assets/css/button-styles.css' , __FILE__ ), array(), PLANET_BUTTONS_VERSION_NUM );
			wp_enqueue_style( 'font-awesome-style', plugins_url( '/assets/css/font-awesome.min.css' , __FILE__ ), array(), PLANET_BUTTONS_VERSION_NUM );
		}
		public function planet_admin_styles()
		{
			wp_enqueue_style( 'planet-buttons-style', plugins_url( '/assets/css/planet-buttons.css' , __FILE__ ), array(), PLANET_BUTTONS_VERSION_NUM);
		}
		
		public function plugin_mce_css( $mce_css )
		{
			if ( ! empty( $mce_css ) )
				$mce_css .= ',';

			$mce_css .= plugins_url( '/assets/css/custom-editor-style.css?ver='.PLANET_BUTTONS_VERSION_NUM , __FILE__ );
			
			return $mce_css;
		}
		
		public function add_mce_plugin( $plugin_array )
		{
		   $plugin_array["planetbuttons"] = plugins_url( '/assets/js/'.PLUGIN_SLUG.'/scripts.min.js?ver='.PLANET_BUTTONS_VERSION_NUM , __FILE__ );
		   return $plugin_array;
		}
		
		function add_plugin_ver_to_js() {
		?>
		<script type="text/javascript">
		/* <![CDATA[ */
		var planet_buttons_ver = <?php echo PLANET_BUTTONS_VERSION_NUM; ?>;/* ]]> */
		</script>
		<?php
		}
		
		
		public function tiny_mce_planet_button()
		{

		   if ( ! current_user_can('edit_posts') && ! current_user_can('edit_pages') )
		   {
			  return;
		   }

		   if ( get_user_option('rich_editing') == 'true' )
		   {
			  add_filter( 'mce_external_plugins', array($this, 'add_mce_plugin') );
			  add_filter( 'mce_buttons', array($this, 'register_button') );
		   }

		}
		
		public function register_button( $buttons )
		{
		   array_push( $buttons, "|", 'planetbuttons' );
		   return $buttons;
		}
	}
}

if ( class_exists( 'PlanetButtons' ) )
{
	global $PlanetButtons;
	$PlanetButtons = new PlanetButtons();
}

